class seta_001_svg {
  String sSeta_001 = '''<svg 
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="143px" height="143px">
                        <path fill-rule="evenodd"  fill="rgb(186, 158, 100)"
                        d="M71.500,0.1 C110.988,0.1 143.1,32.11 143.1,71.500 C143.1,110.988 110.988,143.1 71.500,143.1 C32.11,143.1 0.1,110.988 0.1,71.500 C0.1,32.11 32.11,0.1 71.500,0.1 Z"/>
                    </svg>''';
}
