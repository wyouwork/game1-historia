import 'package:flutter/material.dart';
import 'package:fluttermoji/fluttermoji.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jornadamagica/assets/svg/tela_001_avatar_svg.dart';
import 'package:jornadamagica/assets/svg/tela_001_elementos_svg.dart';
import 'package:jornadamagica/assets/svg/tela_001_bg_svg.dart';
import 'package:jornadamagica/assets/svg/tela_002_avatar_svg.dart';
import 'package:jornadamagica/assets/svg/tela_002_bg_001_svg.dart';
import 'package:jornadamagica/assets/svg/tela_002_bg_002_svg.dart';
import 'package:jornadamagica/assets/svg/tela_002_lencol_svg.dart';
import 'package:jornadamagica/assets/svg/tela_002_mao_svg.dart';
import 'package:jornadamagica/assets/svg/tela_003_avatar_svg.dart';
//import 'package:jornadamagica/assets/svg/tela_002_bg_svg.dart';
//import 'package:jornadamagica/assets/svg/tela_002_lencol_svg.dart';
//import 'package:jornadamagica/assets/svg/tela_002_mao_svg.dart';

import 'package:flutter/foundation.dart' show TargetPlatform;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jornada Mágica',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      home: MyHomePage(title: 'Jornada Mágica'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    TargetPlatform platform = Theme.of(context).platform;
    var isWeb = platform != TargetPlatform.android &&
        platform != TargetPlatform.iOS &&
        platform != TargetPlatform.macOS &&
        platform != TargetPlatform.windows &&
        platform != TargetPlatform.fuchsia;
    var _bgcolor = Color(0xff234a47);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Row(
            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              //),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 32),
                child: FluttermojiCustomizer(
                  scaffoldHeight: 800,
                  showSaveWidget: true,
                  //scaffoldWidth: 100,
                  //scaffoldWidth: isWeb ? 600 : null,
                ),
              ),

              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0),
                  child: Container(
                    color: _bgcolor,
                    //height: MediaQuery.of(context).size.height,
                    height: 400,
                    child: FluttermojiCircleAvatar(
                      radius: 150,
                      backgroundColor: _bgcolor,
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () => Navigator.push(context,
                      new MaterialPageRoute(builder: (context) => PageName())),
                  icon: Image.asset('lib/assets/seta_ocre_001.png'),
                )
              ])
            ],
          ),
        ],
      ),
    );
  }
}

class PageName extends StatelessWidget {
  //const NewPage({Key? key}) : super(key: key);
  TextEditingController nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final newTextTheme = Theme.of(context).textTheme.apply(
          bodyColor: Colors.white,
          displayColor: Colors.pink,
        );

    var _bgcolor = Color(0xff234a47);
    var _txtColor = Color(0xffba9e64);

    return Scaffold(
        appBar: AppBar(),
        backgroundColor: _bgcolor,
        body: Center(
            child: Column(children: <Widget>[
          Container(
            height: 50,
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                border: Border.all(color: _txtColor),
                color: _txtColor),
            child: TextField(
              controller: nameController,
              style: TextStyle(fontSize: 22.0, color: _txtColor),
              decoration: InputDecoration(
                fillColor: _txtColor,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                filled: true,
                hintStyle: TextStyle(fontSize: 20.0, color: Colors.white),
                hintText: 'DIGITE SEU NOME AQUI',
                suffixIcon: IconButton(
                    icon: Image.asset('lib/assets/seta_verde_001.png'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => Page001()));
                    }),
              ),
            ),
          ),
          /* IconButton(
            onPressed: () => Navigator.push(context,
                new MaterialPageRoute(builder: (context) => NewPage())),
            icon: Image.asset('lib/assets/seta_ocre_001.png'),
          )*/
        ])));
  }
}

class Page001 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double dheight = MediaQuery.of(context).size.height * .85;
    double dwidth = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(),
        //backgroundColor: _bgcolor,

        body: new Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                //direction: vertical
                //mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  //new Image.asset("lib/assets/telas/tela_001_001.png"),
                  InkWell(
                    child: Container(
                        child: /*SvgPicture.asset(
                      "lib/assets/telas/tela_001_avatar_001.svg",
                      matchTextDirection: false,
                    ),*/
                            SvgPicture.string(
                      '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' +
                          dwidth.toString() +
                          'px" height="' +
                          dheight.toString() +
                          'px">' +
                          tela_001_bg_svg().sBG_001 +
                          tela_001_elementos_svg().sElementos_001 +
                          tela_001_avatar_svg().sAvatar_001 +
                          ' </svg>',

                      //height: radius * 1.6,
                      //semanticsLabel: "Your Fluttermoji",
                      //placeholderBuilder: (context) => Center(
                      //child: CupertinoActivityIndicator(),
                      //),
                    )),
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => Page002()));
                    },
                  )
                ])));
  }
}

class Page002 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double dheight = MediaQuery.of(context).size.height * .85;
    double dwidth = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(),
        //backgroundColor: _bgcolor,

        body: new Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  InkWell(
                    child: Container(
                        child: SvgPicture.string(
                      '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' +
                          dwidth.toString() +
                          'px" height="' +
                          dheight.toString() +
                          'px">' +
                          tela_002_bg_001_svg().sBG_001 +
                          //tela_002_bg_002_svg().sBG_001 +
                          tela_002_avatar_svg().sAvatar_001 +
                          tela_002_lencol_svg().sLencol_001 +
                          tela_002_mao_svg().sMao_001 +
                          ' </svg>',

                      //height: radius * 1.6,
                      //semanticsLabel: "Your Fluttermoji",
                      //placeholderBuilder: (context) => Center(
                      //child: CupertinoActivityIndicator(),
                      //),
                    )),
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => Page003()));
                    },
                  )
                ])));
  }
}

class Page003 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double dheight = MediaQuery.of(context).size.height * .85;
    double dwidth = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(),
        //backgroundColor: _bgcolor,

        body: Container(
            decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("lib/assets/tela_004.png"),
            fit: BoxFit.cover,
          ),
        )));
  }
}
